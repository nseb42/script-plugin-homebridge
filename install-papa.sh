#!/bin/bash


plugins=(
    "homebridge-mqtt-temperature-log-tasmota"
    "homebridge-min-temperature-log"
    "homebridge-tasmota-sonoff-thermostat"
    "homebridge-max-temperature-log"
    "homebridge-mqtt-switch-tasmota"
    "homebridge-mqtt-temperature-tasmota"
    "homebridge-mqtt-power-consumption-log-tasmota"
    "homebridge-mqtt-humidity-tasmota"
    "homebridge-netatmo"
    "homebridge-ewelink-max"
    "homebridge-ewelink-xs"
)


dpkg -s git > /dev/null
if [ $? -eq 0 ]; then
    echo "git is installed"
else
    echo "Installing git ..."
    sudo apt-get install git
fi

dpkg -s make > /dev/null
if [ $? -eq 0 ]; then
    echo "make is installed"
else
    echo "Installing make ..."
    sudo apt-get install make
fi


for i in ${plugins[@]}
do
    echo -e "----> Installing \e[36m \e[5m $i \e[25m \e[39m ... <----\n"
    sudo npm install -g $i
    if [ $? -eq 0 ]; then
	echo -e "\n \e[1m \e[32m \e[4m $i \e[24m was successfully installed \e[39m \n"
    else
	echo -e "\n \e[31m An error happened installing \e[1m \e[4m $i \e[24m \e[39m \n"
    fi
done
